from fastapi import FastAPI, Response
from escpos.printer import Dummy, Network
import sys
import json

from pydantic import BaseModel
from datetime import datetime

app = FastAPI(
    title="Print Server FastAPI",
    description="Server that receive request for printing",
    version="0.0.1"
)


class Info(BaseModel):
    content: str
    ip_printer: str
    user_name: str


def print_bill(data, address, waiter):
    d = data
    # Crea una instancia de la impresora ficticia
    # printer = Network(str(address))
    # printer.open()
    printer = Dummy()
    # Imprime el encabezado
    printer.set(align='center', bold=False, height=1, width=1)
    printer.text(d["shop_name"]+'\n')
    printer.text(d["shop_address"]+'\n')
    printer.set(align='left', bold=False, height=1, width=1)
    printer.textln('===============================================')
    text = d['state']
    printer.textln(text)
    if d['invoice'] and d['invoice']['resolution']:
        text = "Resolucion de Facturacion # " + \
            str(d['invoice']['resolution']['resolution_number']) \
            + "\nValida desde " + \
            d['invoice']['resolution']['valid_date_time_from'] + \
            " hasta "+str(d['invoice']['resolution']['valid_date_time_to'])
        printer.textln(text)
        printer.ln()
        text = "Factura #: " + d['invoice']['invoice_number']
        printer.textln(text)
    printer.text("Cliente: " + d["party"]+'\n')
    printer.text("CC/NIT: " + d["tax_identifier_code"]+'\n')
    printer.text("Direccion: " + d["address"]+'\n')
    text = 'MESA: ' + str(d['table'] + "\n")
    printer.text(text)
    printer.textln('===============================================')
    printer.ln()
    for line in d["lines"]:
        if line['type'] != 'title':
            text = line['product']
            printer.text(text)
            printer.ln()
            text = str(line['quantity']) + " " + " $" + \
                line["unit_price"] + "\n"
            printer.text(text)

    printer.set(align='right', bold=False, height=1, width=1)
    printer.textln('================================================')
    text = "Descuento Realizado: "+str(d["total_discount"])+"\n"
    printer.text(text)
    text = "Total (sin impuestos): "+str(d["untaxed_amount"])+"\n"
    printer.text(text)
    text = "Impuestos (INC): "+str(d["tax_amount"])+"\n"
    printer.text(text)
    text = "Total: "+str(d["total"])+"\n"
    printer.text(text)
    printer.ln()
    if 'payments' in d.keys():
        printer.textln("Pagos: ")
        printer.textln('================================================')

        for payment in d['payments']:
            text = str(payment["statement"])+" $"+str(payment["amount"])
            printer.textln(text)
    printer.set(align='center', bold=False, height=1, width=1)
    printer.textln('==============================================\n')
    printer.text("Sigue nuestras redes sociales\n")
    printer.text("@bicipizza\n")
    printer.text("Recuerde que la propina es voluntaria.\n")
    printer.text("Gracias por visitarnos, vuelva pronto.\n")
    printer.text("SOFTWARE POTENCIADO POR ONECLUSTER.ORG.\n")
    format_date_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    printer.text(str(format_date_time)+'\n')
    if waiter:
        printer.text("Atendido Por: \n")
        printer.text(str(waiter)+'\n')

    # Corta el papel (solo para impresoras que soportan esta función)
    printer.cut()
    printer.close()
    # Obtiene el contenido del ticket de prueba
    ticket_contenido = printer.output
    # Imprime el contenido en la consola
    sys.stdout.write(ticket_contenido.decode('utf-8'))


def print_customer_order(data, address, waiter):
    d = data
    # Crea una instancia de la impresora ficticia
    # printer = Network(str(address))
    # printer.open()
    printer = Dummy()
    # Imprime el encabezado
    printer.set(align='center', bold=False, height=1, width=1)
    format_date_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    printer.text(str(format_date_time)+'\n')

    if waiter:
        printer.text("Pedido Por: \n")
        printer.text(str(waiter)+'\n')
    printer.set(
        align='center', bold=False, height=2, width=2, custom_size=True
    )
    printer.text('========================\n')
    text = 'MESA: ' + str(d['table'] + "\n")
    printer.text(text)
    printer.text('========================\n')
    printer.set(align='left', bold=False, height=6, width=6)
    combination_pizza = False
    pizza = 0
    for line in d["lines"]:
        if line['type'] != 'title':
            if combination_pizza and pizza < 2:
                printer.set(align='center', bold=False, height=2, width=2, custom_size=True)
                pizza += 1
            elif pizza >= 2:
                combination_pizza = False
                printer.set(
                    align='left', bold=False, height=2, width=2, custom_size=True
                )
            else:
                printer.set(
                    align='left', bold=False, height=2, width=2, custom_size=True
                )

            text = line['product'] + "  " + str(line['quantity']) + "\n"
            printer.text(text)
            if line['description']:
                text = line['description'] + "\n"
                printer.text(text)

            if pizza == 2:
                printer.ln()
                pizza = 0
                combination_pizza = False
        else:
            printer.set(
                align='left', bold=True, height=2, width=2, custom_size=True
            )
            printer.text("\nPIZZA COMBINADA\n")
            combination_pizza = True
            pizza = 0
    # Corta el papel (solo para impresoras que soportan esta función)
    printer.cut()
    printer.close()
    # Obtiene el contenido del ticket de prueba
    ticket_contenido = printer.output

    # Imprime el contenido en la consola
    sys.stdout.write(ticket_contenido.decode('utf-8'))


@app.post("/print_bill")
def print_ticket_bill(info: Info):
    info = dict(info)
    data = info["content"]
    address = info["ip_printer"]
    waiter = info["user_name"]
    data = json.loads(data.replace("'", "\""))
    print_bill(data, address, waiter)

    message = "!Impresión Realizada!"

    return Response(content=message, status_code=200)


@app.post("/order_kitchen")
def print_ticket_file_kitchen(info: Info):
    info = dict(info)
    data = info["content"]
    address = info["ip_printer"]
    waiter = info["user_name"]
    data = json.loads(data.replace("'", "\""))
    print_customer_order(data, address, waiter)

    message = "!Impresión Realizada!"

    return Response(content=message, status_code=200)


@app.post("/order_bar")
def print_ticket_file_bar(info: Info):
    info = dict(info)
    data = info["content"]
    address = info["ip_printer"]
    waiter = info["user_name"]
    data = json.loads(data.replace("'", "\""))
    print_customer_order(data, address, waiter)

    message = "!Impresión Realizada!"

    return Response(content=message, status_code=200)
